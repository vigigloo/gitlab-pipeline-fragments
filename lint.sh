#!/usr/bin/env bash
code=0
for file in {*.auto.yml,proxy/*.yml,registry/*.auto.yml}
do
  CONTENT='{"content": "{\"stages\":[\"docs\",\"build\",\"test\",\"deploy\",\"dast\",\"performance\",\"release\"],\"include\":{\"file\":[\"'$file'\"],\"project\":\"vigigloo/gitlab-pipeline-fragments\",\"ref\":\"'${CI_COMMIT_REF_NAME:-v1}'\"}}"}'
  result=$(echo $CONTENT | curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" "https://$CI_SERVER_HOST/api/v4/projects/$CI_PROJECT_ID/ci/lint" --data @-)
  valid=$(echo $result | dasel -r json '.valid')
  if [ "$valid" = true ]
    then
        echo -e "\033[0;32m$file is valid\033[0m"
    else
        echo -e "\033[0;31m$file is invalid ($(echo $result | dasel -r json '.errors'))\033[0m"
        code=1
  fi
done

exit $code
