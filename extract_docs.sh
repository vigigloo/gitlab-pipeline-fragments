#!/usr/bin/env bash

shopt -s globstar

DOCS_PATH=docs_gen

rm -rf "$DOCS_PATH"
cp -r docs "$DOCS_PATH"

for FRAGMENT_PATH in **/**.yml
do
    echo "$FRAGMENT_PATH"
    mkdir -p "$DOCS_PATH/$(dirname "$FRAGMENT_PATH")"
    cat "$FRAGMENT_PATH" | sed '0,/^$/!d' | sed 's/^# \?//' > "$DOCS_PATH/$FRAGMENT_PATH.md"
done
