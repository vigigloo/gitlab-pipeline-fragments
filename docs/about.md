# About

The CI files in this repository can have one of two functions:
- To be used as a base template
- To be used as-is with minimal configuration

## Templates

Templates are recognised by only containing [hidden jobs](https://docs.gitlab.com/ee/ci/jobs/index.html#hide-jobs).
GitLab won't process jobs that start with a dot, so these can be included in another CI pipeline without any effet.

Template jobs [can be extended](https://docs.gitlab.com/ee/ci/yaml/#extends) to enable them and customize their configuration.

## Ready-to-use jobs

Other CI files have jobs that don't start with dots, and are thus triggerable by GitLab.
These jobs are meant to be functionnal with as little configuration as possible.
